import React from 'react'
import {Card} from 'react-bootstrap'
import "./cards.css"


function cards(props) {
    return (
    <Card className="card">
  <Card.Body>
    <Card.Title className="card_title">{props.Title}</Card.Title>
    <Card.Subtitle className="mb-2 text-muted"><div className="icon-outer">{props.Icon}</div></Card.Subtitle>
    <Card.Text>{props.Text}</Card.Text>
    
  </Card.Body>
</Card>
    )
}


export default cards
