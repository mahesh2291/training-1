import React from 'react'
import {Row,Col, Container} from 'react-bootstrap'
import Graph from './chart'
import Checklist from './checklist';
import './Grid.css'

function Grid() {
    return (
        <div>
        <Container className="service_header">
            <Row>
    <Col><h4>Checklist</h4><Checklist /></Col>
    <Col><h4>Performance</h4><Graph /></Col>
  </Row>
  </Container>
        </div>
    )
}

export default Grid
