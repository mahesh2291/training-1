import React, {useState, useEffect}from 'react';
import { Bar } from 'react-chartjs-2';

import './chart.css'

require('./roundedbar.js')

const Graph = () => {
    const [chartData, setChartData] = useState({})

    const chart = () =>
        setChartData({
            labels: ['Jan', 'Feb', 'Mar', 'Apr'],
            datasets: [
                {
                    label: 'Jan',
                    data: [100, 30, 90, 200],
                    backgroundColor: [
                        '#003f88',
                        '#003f88',
                        '#003f88',
                        '#003f88',
                    ],
                    borderWidth: 4
                },
                {
                    label: 'Feb',
                    data: [70, 11, 180, 40],
                    backgroundColor: [
                        // 'rgba(75, 192, 192, 0.6)',
                        // '#48bfe3',
                        // '#eddcd2',
                        // '#00b4d8',
                        '#fcbf49',
                        '#fcbf49',
                        '#fcbf49',
                        '#fcbf49'
                    ],
                    borderWidth: 4
                },
                {
                    label: 'March',
                    // fillcolor: '#2a9d8f',
                    data: [60, 100,  60, 60],
                    backgroundColor: [
                        // 'rgba(75, 192, 192, 0.6)',
                        // '#48bfe3',
                        // '#b5838d',
                        // '#e85d04'
                        '#e63946',
                        '#e63946',
                        '#e63946',
                        '#e63946'
                    ],
                    borderWidth: 3
                },
                {
                    label: 'Apr',
                    // fillcolor: '#b5838d',
                    data: [],
                    backgroundColor: [
                    ],
                    borderWidth: 4
                }

            ],
        })

    useEffect(() => {
        chart()
    }, [])
    return(
        <div className="graph">
            <Bar  data={chartData} width={2} height={1}
                  options= {{
                      maintainAspectRatio: true,
                      cornerRadius: 8,
                      bodyAlign: 'top',
                      layout: {
                          padding: {
                              bottom: 30,
                              left: 20,
                              right: 30

                          }
                      }
                  }}
                  />
        </div>
    )
}

export default Graph;
