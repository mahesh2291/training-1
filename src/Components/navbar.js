import React from 'react'
import {FormControl,Navbar} from 'react-bootstrap'
import ForumIcon from '@material-ui/icons/Forum';
import SearchIcon from '@material-ui/icons/Search';
import NotificationsIcon from '@material-ui/icons/Notifications';


import "./navbar.css"

function Navbar1() {

    return (
    <div className="nav_head">
        <Navbar className="nav"  expand="lg">

            <div className="search">
                <SearchIcon className="search_icon"/>
                <FormControl  type="text" placeholder="Search" className="searchbar" />
            </div>

            <div className="icons">
            <div className="icon1">
            <NotificationsIcon/>
            </div>
                <div className="icon1">
                <ForumIcon />
                </div>
                
            </div>

        </Navbar>
    </div>
    )
}

export default Navbar1
