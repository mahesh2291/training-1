import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import Graph from '../chart';
import Checklistist from '../checklist';
import Card from '../cards'
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import FavoriteIcon from '@material-ui/icons/Favorite';
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';
import FitnessCenterIcon from '@material-ui/icons/FitnessCenter';
import Paper from '@material-ui/core/Paper';
import SideBar from '../SideBar';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Navbar1 from '../navbar'
import Navbar2 from '../navbar2'


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop:30
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));

export default function CenteredGrid() {
  const classes = useStyles();

  return (


    <div className={classes.root}>
        <Grid container spacing={2}  className={classes.grid} >

            <Grid item xs={2}  md={2}>
                <Router>
                    <SideBar />
                    <Switch>
                        <Route path='/' />
                    </Switch>
                </Router>
            </Grid>

            <Grid item xs={10}  md={10}>
                <Grid container style={{ height:'5%' }} spacing={2}  className={classes.grid} >
                    <Navbar1 />
                </Grid>

                <Grid container style={{ height:'5%' }} spacing={2}  className={classes.grid} >
                    <Navbar2 />
                </Grid>

                <Grid container style={{ height:'40%' }} spacing={2}  className={classes.grid}>
                    <Grid item xs={6} md={3}>
                        <Card Title="SANITY CHECKLIST" Icon={<CheckBoxIcon fontSize="large" />} Text=" Some quick example text to build on the card title and make up the bulk of
      the card's content." />
                    </Grid>

                    <Grid item xs={6} md={3}>
                    <Card Title="HEART BEAST" Icon={<FavoriteIcon fontSize="large" />} Text=" Some quick example text to build on the card title and make up the bulk of
      the card's content." />
                    </Grid>

                    <Grid item xs={6} md={3}>

                    <Card Title="SECURITY CHECKLIST" Icon={<VerifiedUserIcon fontSize="large" />} Text=" Some quick example text to build on the card title and make up the bulk of card's content." />

                    </Grid>

                    <Grid item xs={6} md={3}>

                    <Card Title="DATA CHECKPOINTS" Icon={<FitnessCenterIcon fontSize="large" />} Text=" Some quick example text to build on the card title and make up the bulk of the card's content." />
                    </Grid>

                </Grid>
                <Grid container style={{ height:'50%' }} spacing={2} className={classes.grid}>

                    <Grid item xs={12} md={8}>
                        <Checklistist />
                    </Grid>

                    <Grid item xs={12} md={4}>
                        <Paper style={{'margin-top': '20%', 'height': '70%', 'background': 'transparent linear-gradient(136deg, #00000005 0%, #00000000 100%) 0% 0% no-repeat padding-box',
                            'border-radius': '18px', 'opacity': 1 }} id='paper' variant="outlined" square >
                            <Graph />
                        </Paper>
                    </Grid>

                </Grid>
            </Grid>

        </Grid>
    </div>

  );
}
