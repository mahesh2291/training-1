import React from 'react';
import { Link } from 'react-router-dom';
import * as IoIcons from 'react-icons/io';
import {FiUser} from 'react-icons/fi'

import './SideBar.css'

const SideData = [
    {
        title: 'Dashboard',
        path: '/',
        icon: <IoIcons.IoMdSpeedometer />,
        cName: 'nav-text'
    },
    {
        title: 'Dashboard',
        path: '/',
        icon: <IoIcons.IoMdSpeedometer />,
        cName: 'nav-text'
    },
    {
        title: 'Dashboard',
        path: '/',
        icon: <IoIcons.IoMdSpeedometer />,
        cName: 'nav-text'
    },
    {
        title: 'Dashboard',
        path: '/',
        icon: <IoIcons.IoMdSpeedometer />,
        cName: 'nav-text'
    }
    // {
    //     title: 'Dashboard',
    //     path: '/',
    //     icon: <IoIcons.IoMdSpeedometer />,
    //     cName: 'nav-text'
    // }
]

const SideBar = () => {
    return(
        <>
            <nav className={'nav-menu'}  style={{marginLeft:'10%'}}>
                <Link to='/' >
                    <IoIcons.IoIosRadioButtonOff style={{ color: '#f8f8f8',
                        height: '10%', width: '30%',
                        marginTop:'10%', marginLeft: '30%'
                    }} />

                </Link>
                <br />
                <span className='adminIconSpan'>Logo</span>
                <hr />
                <Link to='/' id='userIcon'>
                    <FiUser style={{ color: '#f8f8f8',
                        height: '10%', width: '20%', marginLeft: '30%'
                    }} />
                </Link>
                <br />
                <span className='adminIconSpan'>TsqAdmin</span>
                <ul className='nav-menu-items'>
                    {SideData.map((item, index) => {
                        return (
                            <div key='sidebar'>
                                <p key={index} className={item.cName}>
                                    <Link to={item.path} style={{color:'#f8f8f8', padding:'1em'}}>
                                        {item.icon}
                                        <span style={{ marginRight:'20%' }} className='menu-title'>{item.title}</span>
                                    </Link>
                                </p>
                            </div>
                        )
                    })}
                </ul>
            </nav>

        </>
    );
}

export default SideBar;
