import React, {useState} from 'react';
import Button from '@material-ui/core/Button';

import "./checklist.css"


function Checklist () {
    const [status1, setStatus1] = useState('running');
    const [status2, setStatus2] = useState('running');
    const [status3, setStatus3] = useState('running');
    const [status4, setStatus4] = useState('running');
    const [status5, setStatus5] = useState('running');
    const [status6, setStatus6] = useState('running');
    const [status7, setStatus7] = useState('running');
    return(
        <div>
            <table>
                <thead>
                    <td>
                        Recent Projects
                    </td>
                    <td>
                        Service Name
                    </td>
                    <td>
                        Type
                    </td>

                    <td>
                        Running Status
                    </td>

                </thead>
                <tr>
                    <td>Service-1</td>
                    <td>Service name</td>
                    <td>Type-1</td>
                    <td><Button size='small' color='info' className='statusBtn' variant='contained' onClick={(e) => {
                        setStatus1(
                            (e.target.value === 'running') ? 'stopped': 'running')
                    }}> {status1} </Button></td>
                </tr>
                <tr>
                    <td>Service-2</td>
                    <td>Service name</td>
                    <td>Type-2</td>
                    <td><Button size='small' color='primary' variant='contained' onClick={(e) => {
                        setStatus2(
                        (e.target.value === 'running') ? 'stopped': 'running')
                    }}> {status2} </Button></td>
                </tr>
                <tr>
                    <td>Service-3</td>
                    <td>Service name</td>
                    <td>Type-3</td>
                    <td><Button size='small' color='primary' variant='contained' onClick={(e) => {
                        setStatus3(
                            (e.target.value === 'running') ? 'stopped': 'running')
                    }}> {status3} </Button></td>
                </tr>
                <tr>
                    <td>Service-4</td>
                    <td>Service name</td>
                    <td>Type-4</td>
                    <td><Button size='small' color='primary' variant='contained' onClick={(e) => {
                        setStatus4(
                            (e.target.value === 'running') ? 'stopped': 'running')
                    }}> {status4} </Button></td>
                </tr>
                <tr>
                    <td>Service-4</td>
                    <td>Service name</td>
                    <td>Type-4</td>
                    <td><Button size='small' color='primary' variant='contained' onClick={(e) => {
                        setStatus5(
                            (e.target.value === 'running') ? 'stopped': 'running')
                    }}> {status5} </Button></td>
                </tr>
                <tr>
                    <td>Service-4</td>
                    <td>Service name</td>
                    <td>Type-4</td>
                    <td><Button size='small' color='primary' variant='contained' onClick={(e) => {
                        setStatus6(
                            (e.target.value === 'running') ? 'stopped': 'running')
                    }}> {status6} </Button></td>
                </tr>
                <tr>
                    <td>Service-4</td>
                    <td>Service name</td>
                    <td>Type-4</td>
                    <td><Button size='small' color='primary' variant='contained' onClick={(e) => {
                        setStatus7(
                            (e.target.value === 'running') ? 'stopped': 'running')
                    }}> {status7} </Button></td>
                </tr>
            </table>
        </div>
    )
}

export default Checklist;
