import React from 'react';

import Layout from './Components/Layout/LayOut1'
import './App.css';
import Grid from '@material-ui/core/Grid';


function App () {

    return (
        <Grid container height='1' width='1'>
            <div >
                <Layout />
            </div>
        </Grid>
    );
}

export default App;

